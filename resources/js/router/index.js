import UserManagement from '../components/UserManagement.vue';
import ExpenseManagement from '../components/ExpenseManagement.vue';

import Dashboard from '../components/Dashboard.vue';
import RoleList from '../components/RoleList.vue';
import UserList from '../components/UserList.vue';
import CategoryList from '../components/CategoryList.vue';
import ExpenseList from '../components/ExpenseList.vue';

const routes = [,
    { path: '/', component: Dashboard },
    { path: '/user-management/roles', component: RoleList, name: 'role.list' },
    { path: '/user-management/users', component: UserList, name: 'user.list' },
    { path: '/expense-management/categories', component: CategoryList, name: 'category.list' },
    { path: '/expense-management/expenses', component: ExpenseList, name: 'expense.list' },
    { path: '/expense-management', component: ExpenseManagement },
    
];

export default routes;