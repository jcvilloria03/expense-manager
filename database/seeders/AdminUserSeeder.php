<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Juan Dela Cruz',
            'email' => 'juan@expensemanager.com',
            'password' => Hash::make('admin_password'),
            'role_id' => 1,
        ]);

        User::create([
            'name' => 'Leo Ocampo',
            'email' => 'leo@expensemanager.com',
            'password' => Hash::make('user_password'),
            'role_id' => 2,
        ]);
    }
}
