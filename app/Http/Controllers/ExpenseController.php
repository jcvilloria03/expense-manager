<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;
use App\Transformers\ExpenseTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses = Expense::with('category')->get();
        //$expenses->formatted_created_at = Carbon::parse($expenses->created_at)->format('Y-m-d');

        $manager = new Manager();
        $resource = new Collection($expenses, new ExpenseTransformer());

        $data = $manager->createData($resource)->toArray();
        
        return response()->json($data, 200);
    }

    public function show($id)
    {
        $expense = Expense::findOrFail($id);

        return response()->json(['data' => $expense], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required|unique:expense_categories',
            'amount' => 'required',
        ]);

        $expense = Expense::create($request->all());
        return response()->json(['data' => $expense], 201);
    }

    public function update(Request $request, $id)
    {
        $expense = Expense::findOrFail($id);

        $request->validate([
            'category_id' => 'required',
            'amount' => 'required',
        ]);

        $expense->update($request->all());
        return response()->json(['data' => $expense], 200);
    }

    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);
        $expense->delete();

        return response()->json(['message' => 'Expense deleted successfully'], 200);
    }
}
