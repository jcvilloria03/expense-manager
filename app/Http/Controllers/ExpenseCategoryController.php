<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExpenseCategory;
use App\Transformers\ExpenseCategoryTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class ExpenseCategoryController extends Controller
{
    public function index()
    {
        $expenseCategories = ExpenseCategory::all();

        $manager = new Manager();
        $resource = new Collection($expenseCategories, new ExpenseCategoryTransformer());

        $data = $manager->createData($resource)->toArray();
        
        return response()->json($data, 200);
    }

    public function show($id)
    {
        $expenseCategory = ExpenseCategory::findOrFail($id);

        return response()->json(['data' => $expenseCategory], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles',
            'description' => 'required',
        ]);

        $expenseCategory = ExpenseCategory::create($request->all());
        return response()->json(['data' => $expenseCategory], 201);
    }

    public function update(Request $request, $id)
    {
        $expenseCategory = ExpenseCategory::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $expenseCategory->update($request->all());
        return response()->json(['data' => $expenseCategory], 200);
    }

    public function destroy($id)
    {
        $expenseCategory = ExpenseCategory::findOrFail($id);
        $expenseCategory->delete();

        return response()->json(['message' => 'Role deleted successfully'], 200);
    }
}
