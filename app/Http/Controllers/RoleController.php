<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Transformers\RoleTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        $manager = new Manager();
        $resource = new Collection($roles, new RoleTransformer());

        $data = $manager->createData($resource)->toArray();
        
        return response()->json($data, 200);
    }

    public function show($id)
    {
        $role = Role::findOrFail($id);

        return response()->json(['data' => $role], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles',
            'description' => 'required',
        ]);

        $role = Role::create($request->all());
        return response()->json(['data' => $role], 201);
    }

    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $role->update($request->all());
        return response()->json(['data' => $role], 200);
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return response()->json(['message' => 'Role deleted successfully'], 200);
    }
}
