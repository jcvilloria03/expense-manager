<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ExpenseCategory;
use Carbon\Carbon;

class ExpenseCategoryTransformer extends TransformerAbstract
{
    public function transform(ExpenseCategory $category)
    {
        return [
            'id' => $category->id,
            'name' => $category->name,
            'description' => $category->description,
            'formatted_created_at' => Carbon::parse($category->created_at)->format('Y-m-d'),
        ];
    }
}