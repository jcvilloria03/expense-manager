<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\User;
use Carbon\Carbon;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role_id' =>  $user->role->id,
            'role_name' =>  $user->role->name,
            'formatted_created_at' => Carbon::parse($user->created_at)->format('Y-m-d'),
        ];
    }
}