<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Role;
use Carbon\Carbon;

class RoleTransformer extends TransformerAbstract
{
    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name,
            'description' => $role->description,
            'formatted_created_at' => Carbon::parse($role->created_at)->format('Y-m-d'),
        ];
    }
}