<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Expense;
use Carbon\Carbon;

class ExpenseTransformer extends TransformerAbstract
{
    public function transform(Expense $expense)
    {
        return [
            'id' => $expense->id,
            'amount' => $expense->amount,
            'entry_date' => $expense->entry_date,
            'category_id' =>  $expense->category->id,
            'category_name' =>  $expense->category->name,
            'formatted_created_at' => Carbon::parse($expense->created_at)->format('Y-m-d'),
        ];
    }
}